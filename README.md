# kbgpg - A GnuPG-compatible commandline wrapper for Keybase

## Introduction

`kbgpg` is a small wrapper CLI for Keybase that emulates a subset of the GnuPG
`gpg` commandline client. It's primary purpose is to allow signing and
verification in programs that only support `gpg` without having to import and
manage keys in your local GnuPG keyring. The main target is `git`, although
other tools are possible (see below for specific usage with tools).

## Installation

`kbgpg` is written in [Rust][rust] and distributed on [crates.io][cargo]; if you
already have a development environment setup just run `cargo install kbgpg`. If
you don't have Rust setup but wish to install this way see [rustup] for the
simplest method of getting Rust installed.

In addition to this there are some binaries
[distributed on Bitbucket][binaries]; currently only Linux amd64 and OS X
architectures are available. These are signed with
[my Keybase ID][tarkasteve]. To use these just download the appropriate binary,
make it executable and put it somewhere on your path.

## Usage

### Git

Usage of `kbgpg` can be enabled globally or on a per-repository basis. The
simplest method is to use `git config`:

    git config --global gpg.program <path-to>/kbgpg

By default Git passes your name/email as the key identifies; Keybase doesn't
understand this and it is ignored; if you only have one key that one will be
used.  If you wish to specify a key you can do so by giving git config the
70-digit key identifier from `keybase pgp list`:

    git config --global user.signingkey 0123456789abcdef....

To sign a commit use `git commit -S ...`. The signed commits will be verified
with kbgpg/Keybase when you do `git log --show-signatures`.

### Leiningen

[Leiningen][lein] will sign both git tags and generated JARs
while [performing a release][lein-deploy]. To use Keybase to do this set the Git
path to `kbgpg`:

    export GIT_LEIN=<path-to>/kbgpg

You can override the default key ID in your `project.clj` or
`~/.lein/profiles.clj`; see the Leiningen [GPG documentation][lein-gpg] for
details.

### Others

Theoretically `kbgpg` should work with any program that uses the `gpg`
commandline and allows you to override the binary path. However gpg's
commandline options have grown organically, and in practice there are several
ways to specify the same thing (`git` and `lein` work very differently for
example). Other tools may require further additions to the `kbgpg`/`gpg`
compatability; use-cases/patches welcome.

## License

Copyright © 2014 Steve Smith

Distributed under GPL v2; see LICENSE for details.


[rust]: https://www.rust-lang.org/
[rustup]: https://www.rustup.rs/
[cargo]: https://crates.io/
[binaries]: https://bitbucket.org/tarkasteve/kbgpg/downloads/
[tarkasteve]: https://keybase.io/tarkasteve
[lein]: https://leiningen.org/
[lein-deploy]: https://github.com/technomancy/leiningen/blob/master/doc/DEPLOY.md
[lein-gpg]: https://github.com/technomancy/leiningen/blob/master/doc/GPG.md
