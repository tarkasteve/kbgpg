
use std::env;
use std::io::{stdout, stderr, Write};
use std::ffi::OsString;
use std::collections::HashSet;
use std::process::{Command, ExitStatus};
use loggerv;
use regex::Regex;

use clap::{App, Arg, ArgGroup};

use errors::*;


// See gpg-interface.c in Git
const SIG_CREATED: &'static str = "\n[GNUPG:] SIG_CREATED \n";
const SIG_GOODSIG: &'static str = "\n[GNUPG:] GOODSIG ";


#[derive(Eq, PartialEq, Hash, Debug)]
pub enum Operation {
    Sign,
    Verify,
}

#[derive(Eq, PartialEq, Hash, Debug)]
pub enum GPGArgs {
    Detach,
    Binary,
    Armor,
    Key(Option<String>),
}

#[derive(Debug)]
pub struct Invocation {
    pub operation: Operation,
    pub args: HashSet<GPGArgs>,
    pub positions: Vec<String>,
    pub status_fd: Option<u32>,
}

impl Default for Invocation {
    fn default() -> Invocation {
        Invocation {
            operation: Operation::Sign,
            args: HashSet::new(),
            positions: Vec::with_capacity(2),
            status_fd: None,
        }
    }
}


pub fn get_gpg_args<I, T>(args: I) -> Result<Invocation>
where
    I: IntoIterator<Item = T>,
    T: Into<OsString> + Clone,
{
    let matches = App::new("GnuPG wrapper for Keybase")

        // Commands
        .arg(Arg::with_name("sign")
             .long("sign")
             .short("s")
             .help("Make a signature.")
             .takes_value(false))

        .arg(Arg::with_name("verify")
             .long("verify")
             .help("Verify a signature.")
             .takes_value(false))

        .group(ArgGroup::with_name("operations")
               .arg("sign")
               .arg("verify"))

        // Options
        .arg(Arg::with_name("detach")
             .long("detach-sign")
             .short("b")
             .help("Make a detached signature.")
             .takes_value(false))

        .arg(Arg::with_name("armor")
             .long("armor")
             .short("a")
             .help("Create ASCII armored output.")
             .takes_value(false))

        .arg(Arg::with_name("status-fd")
             .long("status-fd")
             .help("Write special status strings to the file descriptor n.")
             .takes_value(true))

        .arg(Arg::with_name("user")
             .long("local-user")
             .visible_alias("default-key")
             .short("u")
             .help("Specify key to sign with. (Note: name/email format is ignored; specify a Keybase key-id if necessary.)")
             .takes_value(true))

        .arg(Arg::with_name("keyid-format")
             .long("keyid-format")
             .help("Ignored; kept for Git compatability.")
             .takes_value(true))

        .arg(Arg::with_name("yes")
             .long("yes")
             .help("Assume \"yes\" on most questions (currently ignored).")
             .takes_value(false))

        .arg(Arg::with_name("verbose")
             .long("verbose")
             .short("v")
             .multiple(true)
             .help("Give more information during processing. Use multiple times for more information.")
             .takes_value(false))

        // Positional
        .arg(Arg::with_name("positional")
             .index(1)
             .multiple(true)
             .help("Filename arguments; meaning is command-dependent, see `man gpg` for details."))


        .get_matches_from(args);

    // The Rust test framework seems to call set_logger(), which
    // triggers an error here, so ignore it.
    loggerv::init_with_verbosity(matches.occurrences_of("v")).ok();

    // Set GPG defaults
    let mut inv = Invocation::default();

    // Map GPG args to canonical
    for name in matches.args.keys() {
        match *name {
            "sign" => {
                inv.operation = Operation::Sign;
            }
            "verify" => {
                inv.operation = Operation::Verify;
            }
            "operations" => {
                // Group; ignore
            }

            "armor" => {
                inv.args.insert(GPGArgs::Armor);
            }
            "detach" => {
                inv.args.insert(GPGArgs::Detach);
            }
            "status-fd" => {
                let n = value_t!(matches, "status-fd", u32)?;
                inv.status_fd = Some(n);
            }
            "user" => {
                let key = matches.value_of("user").ok_or(ErrorKind::ValueExpected(
                    name.to_string(),
                ))?;
                inv.args.insert(GPGArgs::Key(Some(key.to_string())));
            }

            "keyid-format" => {
                // Ignored
            }
            "yes" => {
                // Ignored
            }

            "verbose" => {
                // See above
            }

            "positional" => {
                inv.positions = matches.values_of_lossy("positional").ok_or(
                    ErrorKind::ValueExpected(name.to_string()),
                )?;
            }

            _ => {
                bail!(ErrorKind::UnexpectedOption(name.to_string()));
            }
        };
    }

    // Special case; binary is default for signing in GPG, opposite in Keybase.
    if inv.operation == Operation::Sign && !inv.args.contains(&GPGArgs::Armor) {
        inv.args.insert(GPGArgs::Binary);
    }


    Ok(inv)
}

pub fn to_kb_args(inv: &Invocation) -> Result<Vec<String>> {
    let mut args = Vec::new();
    args.push("pgp".to_string());

    // Positional args are command-dependent
    match inv.operation {
        Operation::Sign => {
            args.push("sign".to_string());
            if inv.positions.len() == 1 {
                args.push("--infile".to_string());
                args.push(inv.positions[0].clone());
                // If there's an infile GPG generates an outfile
                let out = inv.positions[0].clone() + ".asc";
                args.push("--outfile".to_string());
                args.push(out);
            }
        }
        Operation::Verify => {
            args.push("verify".to_string());
            // FIXME: This assumes git calling semantics
            args.push("--detached".to_string());
            args.push(inv.positions[0].clone());
            if inv.positions[1] != "-" {
                // stdin is default
                args.push("--infile".to_string());
                args.push(inv.positions[1].clone());
            }
        }
    };

    for ga in inv.args.iter() {
        match *ga {
            GPGArgs::Detach => args.push("--detached".to_string()),
            GPGArgs::Binary => args.push("--binary".to_string()),
            GPGArgs::Armor => {}  // Default for Keybase
            GPGArgs::Key(Some(ref key)) => {
                // Only use if it's a Keybase PGP-format key
                if Regex::new("[[:xdigit:]][70]")?.is_match(key) {
                    args.push("--key".to_string());
                    args.push(key.clone());
                };
            }
            _ => {}           // Ignored
        };
    }

    Ok(args)
}

pub fn invoke_kb(args: &Vec<String>) -> Result<ExitStatus> {
    let kb = "keybase"; // FIXME: Get from settings/env

    debug!("Invoking keybase binary {:?} with {:?}", kb, args);
    let result = Command::new(kb).args(args.as_slice()).status()?;

    Ok(result)
}

fn write_status(inv: &Invocation, msg: &str) -> Result<()> {
    match inv.status_fd {
        // We could convert to a raw FD, but I'm not sure that
        // anything except these two make sense...
        Some(1) => {
            stdout().write(msg.as_bytes())?;
        }
        Some(2) => {
            stderr().write(msg.as_bytes())?;
        }
        None => {}
        Some(n) => bail!(ErrorKind::UnexpectedOption(n.to_string())),
    };
    Ok(())
}

fn final_status(inv: &Invocation, status: &ExitStatus) -> Result<()> {
    // Assume return code is sufficient to signal failure.
    if inv.status_fd.is_none() || !status.success() {
        return Ok(());
    }

    // Git expects status messages for success
    match inv.operation {
        Operation::Sign => {
            write_status(&inv, SIG_CREATED)?;
        }
        Operation::Verify => {
            write_status(&inv, SIG_GOODSIG)?;
        }
    }

    Ok(())
}

pub fn run() -> Result<i32> {
    let inv = get_gpg_args(env::args())?;
    let kargs = to_kb_args(&inv)?;
    let status = invoke_kb(&kargs)?;

    final_status(&inv, &status)?;

    let ret = match status.code() {
        Some(c) => c,
        None => 0,
    };

    Ok(ret)
}


//////////////////////////////////////////////////////////////////////////////
// Tests
//////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
    use super::*;

    const VALID_KEY: &'static str = "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef012345";

    #[test]
    fn test_sign() {
        let inv = get_gpg_args(vec!["kbgpg", "--sign"]).unwrap();
        assert_eq!(1, inv.args.len());
        assert_eq!(inv.operation, Operation::Sign);
        assert!(inv.args.contains(&GPGArgs::Binary));
    }

    #[test]
    fn test_verify() {
        let inv = get_gpg_args(vec!["kbgpg", "--verify"]).unwrap();
        assert_eq!(0, inv.args.len());
        assert_eq!(inv.operation, Operation::Verify);
    }

    #[test]
    fn test_detach() {
        let inv = get_gpg_args(vec!["kbgpg", "-s", "--detach-sign"]).unwrap();
        assert_eq!(2, inv.args.len());
        assert!(inv.args.contains(&GPGArgs::Detach));
        assert!(inv.args.contains(&GPGArgs::Binary));
    }

    #[test]
    fn test_status() {
        let inv = get_gpg_args(vec!["kbgpg", "-s", "--status-fd=2"]).unwrap();
        assert_eq!(1, inv.args.len());
        assert!(inv.args.contains(&GPGArgs::Binary));
        assert_eq!(2, inv.status_fd.unwrap());
    }

    #[test]
    fn test_armor() {
        let inv = get_gpg_args(vec!["kbgpg", "-s", "-a"]).unwrap();
        assert_eq!(1, inv.args.len());
        assert!(inv.args.contains(&GPGArgs::Armor));
    }

    #[test]
    fn test_key() {
        let inv = get_gpg_args(vec!["kbgpg", "-s", "-u", "1234"]).unwrap();
        assert_eq!(2, inv.args.len());
        assert!(inv.args.contains(&GPGArgs::Key(Some("1234".to_string()))));
        assert!(inv.args.contains(&GPGArgs::Binary));
    }

    #[test]
    fn test_arg_conv_default() {
        let inv = Invocation::default();
        let args = to_kb_args(&inv).unwrap();

        assert_eq!(2, args.len());
        assert_eq!("pgp", args[0]);
        assert_eq!("sign", args[1]);
    }

    #[test]
    fn test_arg_conv_ascii() {
        let inv = get_gpg_args(vec!["kbgpg", "-s", "-a"]).unwrap();
        let mut args = to_kb_args(&inv).unwrap();
        args.sort();

        assert_eq!(2, args.len());
        assert_eq!("pgp", args[0]);
        assert_eq!("sign", args[1]);
    }

    #[test]
    fn test_arg_conv_git_default() {
        let inv = get_gpg_args(vec![
            "kbgpg",
            "--status-fd=2",
            "-bsau",
            "Steve Smith <tarkasteve@gmail.com>",
        ]).unwrap();
        let mut args = to_kb_args(&inv).unwrap();
        args.sort();

        assert_eq!(3, args.len());
        assert_eq!("--detached", args[0]);
        assert_eq!("pgp", args[1]);
        assert_eq!("sign", args[2]);
    }

    #[test]
    fn test_arg_conv_git_with_key() {
        let inv = get_gpg_args(vec!["kbgpg", "--status-fd=2", "-bsau", VALID_KEY]).unwrap();
        let mut args = to_kb_args(&inv).unwrap();
        args.sort();

        assert_eq!(5, args.len());
        assert_eq!("--detached", args[0]);
        assert_eq!("--key", args[1]);
        assert_eq!(VALID_KEY, args[2]);
        assert_eq!("pgp", args[3]);
        assert_eq!("sign", args[4]);
    }

    #[test]
    fn test_arg_verify_git_default() {
        let inv = get_gpg_args(vec![
            "kbgpg",
            "--status-fd=1",
            "--keyid-format=long",
            "--verify",
            "z_pos1",
            "-",
        ]).unwrap();
        let mut args = to_kb_args(&inv).unwrap();
        args.sort();

        assert_eq!(4, args.len());
        assert_eq!("--detached", args[0]);
        assert_eq!("pgp", args[1]);
        assert_eq!("verify", args[2]);
        assert_eq!("z_pos1", args[3]);
    }


    #[test]
    fn test_arg_conv_lein_with_key() {
        let inv = get_gpg_args(vec![
            "kbgpg",
            "--yes",
            "-ab",
            "--default-key",
            VALID_KEY,
            "--",
            "file.txt",
        ]).unwrap();
        let mut args = to_kb_args(&inv).unwrap();
        args.sort();

        assert_eq!(9, args.len());
        assert_eq!("--detached", args[0]);
        assert_eq!("--infile", args[1]);
        assert_eq!("--key", args[2]);
        assert_eq!("--outfile", args[3]);
        assert_eq!(VALID_KEY, args[4]);
        assert_eq!("file.txt", args[5]);
        assert_eq!("file.txt.asc", args[6]);
        assert_eq!("pgp", args[7]);
        assert_eq!("sign", args[8]);
    }

}
