
#[macro_use]
extern crate log;
extern crate loggerv;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate error_chain;
extern crate regex;

mod errors;
mod kbgpg;


quick_main!(kbgpg::run);
