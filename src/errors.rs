use std::io;
use clap;
use log;
use regex;

// Create the Error, ErrorKind, ResultExt, and Result types
error_chain! {
    errors {
        ValueExpected(e: String) {
            display("Expected value for option: {}", e)
        }
        UnexpectedOption(e: String) {
            display("Unexpected commandline option: {}", e)
        }
    }

    foreign_links {
        OptionError(clap::Error);
        LoggerError(log::SetLoggerError);
        IOError(io::Error);
        RegexError(regex::Error);
    }
}
